@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Daftar Customer</div>

                <div class="card-body">
                <table class="table table-bordered">
                    <tr>
                        <th>Nama</th>
                        <th>Alamat</th>
                    </tr>
                    <tr>
                        @foreach($customers as $customer)
                        <td>{{ $customer->name }}</td>
                        <td>{{ $customer->address }}</td>
                        @endforeach
                    </tr>

                </table>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
